package com.udicity.shams.quiztime;

import android.content.Context;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import static android.R.attr.id;


public class MainActivity extends AppCompatActivity {

    private float Score = 0;
    private String Name;
    private static final String TAG = MainActivity.class.getSimpleName();
    private int numberOfQuestionsAnswered;
    private boolean questionOneUserAnswer;
    private boolean questionTwoUserAnswer;
    private boolean questionThreeUserAnswer;
    private boolean questionFourUserAnswer;
    private boolean questionFiveUserAnswer;
    private boolean questionSixUserAnswer;
    private boolean questionSevenUserAnswer;
    private boolean questionEightUserAnswer;
    private boolean questionNineUserAnswer;
    private boolean questionTenUserAnswer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        questionThree();
        questionSix();
        Log.i(TAG, "Iam On Create Method");
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));

    }

    public void signName(View view) {
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        EditText nameEditText = (EditText) findViewById(R.id.edit_text_get_name);
        Name = nameEditText.getText().toString();
        if (Name.matches("")) {
            Toast.makeText(getApplicationContext(), "enter your Name Please ", Toast.LENGTH_LONG).show();
        } else {
            nameEditText.setVisibility(View.GONE);
            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(nameEditText.getWindowToken(), 0);
            Button nameButton = (Button) findViewById(R.id.button_sign_name);
            nameButton.setVisibility(View.GONE);
            TextView nameTextView = (TextView) findViewById(R.id.text_view_name);
            nameTextView.setText(Name);
        }
    }

    //Question One Enabled View
    public void questionOneEnabledViews() {
        RadioButton radioOne = (RadioButton) findViewById(R.id.radio_button_one_consort_question_one);
        RadioButton radioTwo = (RadioButton) findViewById(R.id.radio_button_two_friend_question_one);
        RadioButton radioThree = (RadioButton) findViewById(R.id.radio_button_three_accessory_question_one);
        RadioButton radioFour = (RadioButton) findViewById(R.id.radio_button_four_comrade_question_one);
        RadioButton radioFive = (RadioButton) findViewById(R.id.radio_button_five_follower_question_one);
        radioOne.setEnabled(false);
        radioTwo.setEnabled(false);
        radioThree.setEnabled(false);
        radioFour.setEnabled(false);
        radioFive.setEnabled(false);
    }

    public void scrollToView(int id)
    {
        final ScrollView scrollView = (ScrollView)findViewById(R.id.scroll_view_id);
        final LinearLayout linearLayout=(LinearLayout)findViewById(id);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0 , linearLayout.getBottom());
            }
        });
    }

    //Start Of Question One
    public void questionOne(View view) {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        boolean radioChecked = ((RadioButton) view).isChecked();
        RadioButton radioOne = (RadioButton) findViewById(R.id.radio_button_one_consort_question_one);
        RadioButton radioTwo = (RadioButton) findViewById(R.id.radio_button_two_friend_question_one);
        RadioButton radioThree = (RadioButton) findViewById(R.id.radio_button_three_accessory_question_one);
        RadioButton radioFour = (RadioButton) findViewById(R.id.radio_button_four_comrade_question_one);
        RadioButton radioFive = (RadioButton) findViewById(R.id.radio_button_five_follower_question_one);
        String questionOneCorrect = "accessory";
        switch (view.getId()) {
            case R.id.radio_button_one_consort_question_one:
                if (radioChecked) {
                    if (questionOneCorrect.equalsIgnoreCase(radioOne.getText().toString())) {
                        questionOneUserAnswer = true;
                        radioOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionOneEnabledViews();
                        displayFullScore();
                    } else {
                        questionOneUserAnswer = false;
                        radioOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionOneEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_two_friend_question_one:
                if (radioChecked) {
                    if (questionOneCorrect.equalsIgnoreCase(radioTwo.getText().toString())) {
                        questionOneUserAnswer = true;
                        radioTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionOneEnabledViews();
                        displayFullScore();
                    } else {

                        questionOneUserAnswer = false;
                        radioTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionOneEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_three_accessory_question_one:
                if (radioChecked) {
                    if (questionOneCorrect.equalsIgnoreCase(radioThree.getText().toString())) {
                        questionOneUserAnswer = true;
                        radioThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionOneEnabledViews();
                        displayFullScore();
                    } else {
                        questionOneUserAnswer = false;
                        radioThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionOneEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_four_comrade_question_one:
                if (radioChecked) {
                    if (questionOneCorrect.equalsIgnoreCase(radioFour.getText().toString())) {
                        questionOneUserAnswer = true;
                        radioFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionOneEnabledViews();
                        displayFullScore();
                    } else {
                        questionOneUserAnswer = false;
                        radioFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionOneEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_five_follower_question_one:
                if (radioChecked) {
                    if (questionOneCorrect.equalsIgnoreCase(radioFive.getText().toString())) {
                        questionOneUserAnswer = true;
                        radioFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionOneEnabledViews();
                        displayFullScore();
                    } else {
                        questionOneUserAnswer = false;
                        radioFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionOneEnabledViews();
                    }
                }

                break;
        }
        scrollToView( R.id.question_one_layout);
    }
    //End Of Question One

    //Question Two Enabled Views
    public void questionTwoEnabledViews() {
        CheckBox checkOne = (CheckBox) findViewById(R.id.checkbox_one_question_two);
        CheckBox checkTwo = (CheckBox) findViewById(R.id.checkbox_two_question_two);
        CheckBox checkThree = (CheckBox) findViewById(R.id.checkbox_three_question_two);
        CheckBox checkFour = (CheckBox) findViewById(R.id.checkbox_four_question_two);
        CheckBox checkFive = (CheckBox) findViewById(R.id.checkbox_five_question_two);
        CheckBox checkSix = (CheckBox) findViewById(R.id.checkbox_six_question_two);
        Button submitButton = (Button) findViewById(R.id.button_one_submit_question_two);
        checkOne.setEnabled(false);
        checkTwo.setEnabled(false);
        checkThree.setEnabled(false);
        checkFour.setEnabled(false);
        checkFive.setEnabled(false);
        checkSix.setEnabled(false);
        submitButton.setEnabled(false);
    }

    //Start Of Question Two
    public void questionTwo(View view) {
        numberOfQuestionsAnswered++;
        int numberOfCheckBoxs = 0;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        CheckBox checkOne = (CheckBox) findViewById(R.id.checkbox_one_question_two);
        boolean isCheckedOne = checkOne.isChecked();
        CheckBox checkTwo = (CheckBox) findViewById(R.id.checkbox_two_question_two);
        boolean isCheckedTwo = checkTwo.isChecked();
        CheckBox checkThree = (CheckBox) findViewById(R.id.checkbox_three_question_two);
        boolean isCheckedThree = checkThree.isChecked();
        CheckBox checkFour = (CheckBox) findViewById(R.id.checkbox_four_question_two);
        boolean isCheckedFour = checkFour.isChecked();
        CheckBox checkFive = (CheckBox) findViewById(R.id.checkbox_five_question_two);
        boolean isCheckedFive = checkFive.isChecked();
        CheckBox checkSix = (CheckBox) findViewById(R.id.checkbox_six_question_two);
        boolean isCheckedSix = checkSix.isChecked();
        //String questionOneCorrectPartOne = "literal";
        //String questionOneCorrectPartTwo = "verbatim";
        if (isCheckedOne)
            numberOfCheckBoxs++;
        if (isCheckedTwo)
            numberOfCheckBoxs++;
        if (isCheckedThree)
            numberOfCheckBoxs++;
        if (isCheckedFour)
            numberOfCheckBoxs++;
        if (isCheckedFive)
            numberOfCheckBoxs++;
        if (isCheckedSix)
            numberOfCheckBoxs++;

        if (numberOfCheckBoxs > 2) {
            Toast.makeText(getApplicationContext(), "Choose Only Two Answers !!", Toast.LENGTH_LONG).show();
        } else {
            if (isCheckedOne) {
                if (isCheckedTwo) {
                    // One And Two Are Checked
                    questionTwoUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                } else if (isCheckedThree) {
                    //One And Three Are Checked
                    questionTwoUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else if (isCheckedFour) {
                    //One And Four Are Checked
                    questionTwoUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                } else if (isCheckedFive) {
                    //One And Five Are Checked
                    questionTwoUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                } else if (isCheckedSix) {
                    //One And Six Are Checked
                    questionTwoUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else {
                    //only One Has Checked
                    questionTwoUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                }

            } else if (isCheckedTwo) {
                if (isCheckedThree) {
                    // Two And Three Are Checked
                    questionTwoUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else if (isCheckedFour) {
                    // Two And Four Are Checked
                    questionTwoUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                } else if (isCheckedFive) {
                    // Two And Five Are Checked
                    questionTwoUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                } else if (isCheckedSix) {
                    // Two And Six Are Checked
                    questionTwoUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else {
                    //Only Two Has Checked
                    questionTwoUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                }

            } else if (isCheckedThree) {
                if (isCheckedFour) {
                    //Three And Four Are Checked
                    questionTwoUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else if (isCheckedFive) {
                    //Three And Five Are Checked
                    questionTwoUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else if (isCheckedSix) {
                    //Three And Six Are Checked
                    questionTwoUserAnswer = true;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayFullScore();
                } else {
                    //Only Three Has Checked
                    questionTwoUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                }

            } else if (isCheckedFour) {
                if (isCheckedFive) {
                    //Four And Five Are Checked
                    questionTwoUserAnswer = false;
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                } else if (isCheckedSix) {
                    //Four And Six Are Checked
                    questionTwoUserAnswer = false;
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else {
                    //Only Four Has Checked
                    questionTwoUserAnswer = false;
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                }
            } else if (isCheckedFive) {
                if (isCheckedSix) {
                    //Five And Sex Has Checked
                    questionTwoUserAnswer = false;
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTwoEnabledViews();
                    displayHalfScore();
                } else {
                    //Only Five Has Checked
                    questionTwoUserAnswer = false;
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTwoEnabledViews();
                }

            } else if (isCheckedSix) {
                questionTwoUserAnswer = false;
                checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                questionTwoEnabledViews();
                displayHalfScore();
            }
            scrollToView( R.id.question_two_layout);
        }
    }
    //End Of Question Two

    //Question Three Enabled Views
    public void questionThreeEnabledViews() {
        final Button answerOfButtonOne = (Button) findViewById(R.id.button_one_question_three);
        final Button answerOfButtonTwo = (Button) findViewById(R.id.button_two_question_three);
        final Button answerOfButtonThree = (Button) findViewById(R.id.button_three_question_three);
        final Button answerOfButtonFour = (Button) findViewById(R.id.button_four_question_three);
        answerOfButtonOne.setEnabled(false);
        answerOfButtonTwo.setEnabled(false);
        answerOfButtonThree.setEnabled(false);
        answerOfButtonFour.setEnabled(false);
    }

    //Start Of Question Three
    public void questionThree() {
        // numberOfQuestionsAnswered++;
        // Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        final Button answerOfButtonOne = (Button) findViewById(R.id.button_one_question_three);
        final Button answerOfButtonTwo = (Button) findViewById(R.id.button_two_question_three);
        final Button answerOfButtonThree = (Button) findViewById(R.id.button_three_question_three);
        final Button answerOfButtonFour = (Button) findViewById(R.id.button_four_question_three);
        final String CorrectAnswer = "740 m.p.h";


        answerOfButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfQuestionsAnswered++;
                Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
                if (CorrectAnswer.equalsIgnoreCase(answerOfButtonOne.getText().toString())) {
                    questionThreeUserAnswer = true;
                    answerOfButtonOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionThreeEnabledViews();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(answerOfButtonOne.getText().toString())) {
                    questionThreeUserAnswer = false;
                    answerOfButtonOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionThreeEnabledViews();
                }
            }
        });

        answerOfButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfQuestionsAnswered++;
                Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
                if (CorrectAnswer.equalsIgnoreCase(answerOfButtonTwo.getText().toString())) {
                    questionThreeUserAnswer = true;
                    answerOfButtonTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionThreeEnabledViews();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(answerOfButtonTwo.getText().toString())) {
                    questionThreeUserAnswer = false;
                    answerOfButtonTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionThreeEnabledViews();
                }
            }
        });

        answerOfButtonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfQuestionsAnswered++;
                Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
                if (CorrectAnswer.equalsIgnoreCase(answerOfButtonThree.getText().toString())) {
                    questionThreeUserAnswer = true;
                    answerOfButtonThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionThreeEnabledViews();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(answerOfButtonThree.getText().toString())) {
                    questionThreeUserAnswer = false;
                    answerOfButtonThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionThreeEnabledViews();
                }
            }
        });

        answerOfButtonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numberOfQuestionsAnswered++;
                Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
                if (CorrectAnswer.equalsIgnoreCase(answerOfButtonFour.getText().toString())) {
                    questionThreeUserAnswer = true;
                    answerOfButtonFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionThreeEnabledViews();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(answerOfButtonFour.getText().toString())) {
                    questionThreeUserAnswer = false;
                    answerOfButtonFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionThreeEnabledViews();
                }
            }
        });
       // scrollToView( R.id.question_three_layout);
    }

    //Question Four Enabled Views
    public void questionFourEnabledViews() {
        RadioButton radioOne = (RadioButton) findViewById(R.id.radio_button_one_famous_question_four);
        RadioButton radioTwo = (RadioButton) findViewById(R.id.radio_button_two_illustrious_question_four);
        RadioButton radioThree = (RadioButton) findViewById(R.id.radio_button_three_acclaimed_question_four);
        RadioButton radioFour = (RadioButton) findViewById(R.id.radio_button_four_fabulous_question_four);
        RadioButton radioFive = (RadioButton) findViewById(R.id.radio_button_five_noteworthy_question_four);
        radioOne.setEnabled(false);
        radioTwo.setEnabled(false);
        radioThree.setEnabled(false);
        radioFour.setEnabled(false);
        radioFive.setEnabled(false);
    }

    //Start Of Question Four
    public void questionFour(View view) {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        boolean radioChecked = ((RadioButton) view).isChecked();
        RadioButton radioOne = (RadioButton) findViewById(R.id.radio_button_one_famous_question_four);
        RadioButton radioTwo = (RadioButton) findViewById(R.id.radio_button_two_illustrious_question_four);
        RadioButton radioThree = (RadioButton) findViewById(R.id.radio_button_three_acclaimed_question_four);
        RadioButton radioFour = (RadioButton) findViewById(R.id.radio_button_four_fabulous_question_four);
        RadioButton radioFive = (RadioButton) findViewById(R.id.radio_button_five_noteworthy_question_four);
        String questionFourCorrect = "fabulous";


        switch (view.getId()) {
            case R.id.radio_button_one_famous_question_four:
                if (radioChecked) {
                    if (questionFourCorrect.equalsIgnoreCase(radioOne.getText().toString())) {
                        questionFourUserAnswer = true;
                        radioOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionFourEnabledViews();
                        displayFullScore();
                    } else {

                        questionFourUserAnswer = false;
                        radioOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionFourEnabledViews();
                    }

                }
                break;
            case R.id.radio_button_two_illustrious_question_four:
                if (radioChecked) {
                    if (questionFourCorrect.equalsIgnoreCase(radioTwo.getText().toString())) {
                        questionFourUserAnswer = true;
                        radioTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionFourEnabledViews();
                        displayFullScore();
                    } else {

                        questionFourUserAnswer = false;
                        radioTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionFourEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_three_acclaimed_question_four:
                if (radioChecked) {
                    if (questionFourCorrect.equalsIgnoreCase(radioThree.getText().toString())) {
                        questionFourUserAnswer = true;
                        radioThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionFourEnabledViews();
                        displayFullScore();
                    } else {

                        questionFourUserAnswer = false;
                        radioThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionFourEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_four_fabulous_question_four:
                if (radioChecked) {
                    if (questionFourCorrect.equalsIgnoreCase(radioFour.getText().toString())) {
                        questionFourUserAnswer = true;
                        radioFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionFourEnabledViews();
                        displayFullScore();
                    } else {

                        questionFourUserAnswer = false;
                        radioFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionFourEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_five_noteworthy_question_four:
                if (radioChecked) {
                    if (questionFourCorrect.equalsIgnoreCase(radioFive.getText().toString())) {
                        questionFourUserAnswer = true;
                        radioFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionFourEnabledViews();
                        displayFullScore();
                    } else {

                        questionFourUserAnswer = false;
                        radioFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionFourEnabledViews();
                    }
                }
                break;
        }
        scrollToView( R.id.question_four_layout);
    }
    // End Of Question Four

    //Question five Enabled Views
    public void questionFiveEnabledViews() {
        EditText editTextNumberOne = (EditText) findViewById(R.id.edit_text_one_question_five);
        EditText editTextNumberTwo = (EditText) findViewById(R.id.edit_text_two_question_five);
        Button submitButton = (Button) findViewById(R.id.button_one_question_five);
        editTextNumberOne.setEnabled(false);
        editTextNumberTwo.setEnabled(false);
        submitButton.setEnabled(false);
    }

    //Start Of Question Five
    public void questionFive(View view) {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        int numberOne = 75;
        int numberTwo = 26;
        EditText editTextNumberOne = (EditText) findViewById(R.id.edit_text_one_question_five);
        String editNumberOne = editTextNumberOne.getText().toString();
        EditText editTextNumberTwo = (EditText) findViewById(R.id.edit_text_two_question_five);
        String editNumberTwo = editTextNumberTwo.getText().toString();

        if (editNumberOne.matches("")) {
            if (editNumberTwo.matches("")) {
                Toast.makeText(getApplicationContext(), "enter The your Answers !!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "enter The first Answer !!", Toast.LENGTH_SHORT).show();
            }
        } else if (editNumberTwo.matches("")) {
            Toast.makeText(getApplicationContext(), "enter The Second Answer !!", Toast.LENGTH_SHORT).show();
        } else {
            if (numberOne == Integer.parseInt(editNumberOne)) {
                if (numberTwo == Integer.parseInt(editNumberTwo)) {
                    questionFiveUserAnswer = true;
                    editTextNumberOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    editTextNumberTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionFiveEnabledViews();
                    displayFullScore();
                } else {
                    questionFiveUserAnswer = false;
                    editTextNumberOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    editTextNumberTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionFiveEnabledViews();
                    displayHalfScore();
                }
            } else if (numberTwo == Integer.parseInt(editNumberTwo)) {
                questionFiveUserAnswer = false;
                editTextNumberOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                editTextNumberTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                questionFiveEnabledViews();
                displayHalfScore();
            } else {
                questionFiveUserAnswer = false;
                editTextNumberOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                editTextNumberTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                questionFiveEnabledViews();
            }
            scrollToView( R.id.question_five_layout);
        }
    }

    //Question Six Enabled Views
    public void questionSixEnabledView() {
        final Button button1 = (Button) findViewById(R.id.button_one_squirrel_question_six);
        final Button button2 = (Button) findViewById(R.id.button_two_tortoise_question_six);
        final Button button3 = (Button) findViewById(R.id.button_three_tigress_question_six);
        final Button button4 = (Button) findViewById(R.id.button_four_wildebeest_question_six);
        final Button button5 = (Button) findViewById(R.id.button_five_platypus_question_six);
        final Button button6 = (Button) findViewById(R.id.button_six_aardvark_question_six);

        button1.setEnabled(false);
        button2.setEnabled(false);
        button3.setEnabled(false);
        button4.setEnabled(false);
        button6.setEnabled(false);
        button5.setEnabled(false);
        button6.setEnabled(false);
    }

    //Start Of Question Six
    public void questionSix() {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        final Button button1 = (Button) findViewById(R.id.button_one_squirrel_question_six);
        final Button button2 = (Button) findViewById(R.id.button_two_tortoise_question_six);
        final Button button3 = (Button) findViewById(R.id.button_three_tigress_question_six);
        final Button button4 = (Button) findViewById(R.id.button_four_wildebeest_question_six);
        final Button button5 = (Button) findViewById(R.id.button_five_platypus_question_six);
        final Button button6 = (Button) findViewById(R.id.button_six_aardvark_question_six);
        final String CorrectAnswer = "tortoise";
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CorrectAnswer.equalsIgnoreCase(button1.getText().toString())) {
                    questionSixUserAnswer = true;
                    button1.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.rightAnswer));
                    questionSixEnabledView();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(button1.getText().toString())) {
                    questionSixUserAnswer = false;
                    button1.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.wrongAnswer));
                    questionSixEnabledView();
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CorrectAnswer.equalsIgnoreCase(button2.getText().toString())) {
                    questionSixUserAnswer = true;
                    button2.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.rightAnswer));
                    questionSixEnabledView();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(button2.getText().toString())) {
                    questionSixUserAnswer = false;
                    button2.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.wrongAnswer));
                    questionSixEnabledView();
                }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CorrectAnswer.equalsIgnoreCase(button3.getText().toString())) {
                    questionSixUserAnswer = true;
                    button3.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.rightAnswer));
                    questionSixEnabledView();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(button3.getText().toString())) {
                    questionSixUserAnswer = false;
                    button3.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.wrongAnswer));
                    questionSixEnabledView();
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CorrectAnswer.equalsIgnoreCase(button4.getText().toString())) {
                    questionSixUserAnswer = true;
                    button4.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.rightAnswer));
                    questionSixEnabledView();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(button4.getText().toString())) {
                    questionSixUserAnswer = false;
                    button4.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.wrongAnswer));
                    questionSixEnabledView();
                }
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CorrectAnswer.equalsIgnoreCase(button5.getText().toString())) {
                    questionSixUserAnswer = true;
                    button5.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.rightAnswer));
                    questionSixEnabledView();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(button5.getText().toString())) {
                    questionSixUserAnswer = false;
                    button5.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.wrongAnswer));
                    questionSixEnabledView();
                }
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CorrectAnswer.equalsIgnoreCase(button6.getText().toString())) {
                    questionSixUserAnswer = true;
                    button6.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.rightAnswer));
                    questionSixEnabledView();
                    displayFullScore();
                } else if (!CorrectAnswer.equalsIgnoreCase(button6.getText().toString())) {
                    questionSixUserAnswer = false;
                    button6.setTextColor(ContextCompat.getColorStateList(getApplication(), R.color.wrongAnswer));
                    questionSixEnabledView();
                }
            }
        });
      //  scrollToView( R.id.question_six_layout);
    }
    //End Of Question Six

    //Start Of Question Seven
    public void questionSeven(View view) {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        int numberAnswer = 76;
        EditText editText = (EditText) findViewById(R.id.edit_text_one_question_seven);
        String numberOfEditText = editText.getText().toString();
        Button submitButton = (Button) findViewById(R.id.button_one_question_seven);

        if (numberOfEditText.matches("")) {
            Toast.makeText(getApplicationContext(), "enter The Answer please", Toast.LENGTH_LONG).show();
        } else {
            if (numberAnswer == Integer.parseInt(numberOfEditText)) {
                questionSevenUserAnswer = true;
                editText.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                editText.setEnabled(false);
                submitButton.setEnabled(false);
                displayFullScore();
            } else {
                questionSevenUserAnswer = false;
                editText.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                editText.setEnabled(false);
                submitButton.setEnabled(false);
            }
            scrollToView( R.id.question_seven_layout);
        }
    }

    //Question Eight EnabledViews
    public void questionEightEnabledViews() {
        RadioButton radioOne = (RadioButton) findViewById(R.id.radio_button_one_ginger_question_eight);
        RadioButton radioTwo = (RadioButton) findViewById(R.id.radio_button_two_humane_question_eight);
        RadioButton radioThree = (RadioButton) findViewById(R.id.radio_button_three_abacus_question_eight);
        RadioButton radioFour = (RadioButton) findViewById(R.id.radio_button_four_yogurt_question_eight);
        RadioButton radioFive = (RadioButton) findViewById(R.id.radio_button_five_sector_question_eight);

        radioOne.setEnabled(false);
        radioTwo.setEnabled(false);
        radioThree.setEnabled(false);
        radioFour.setEnabled(false);
        radioFive.setEnabled(false);
    }

    //Start Of Question Eight
    public void questionEight(View view) {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        boolean radioChecked = ((RadioButton) view).isChecked();
        RadioButton radioOne = (RadioButton) findViewById(R.id.radio_button_one_ginger_question_eight);
        RadioButton radioTwo = (RadioButton) findViewById(R.id.radio_button_two_humane_question_eight);
        RadioButton radioThree = (RadioButton) findViewById(R.id.radio_button_three_abacus_question_eight);
        RadioButton radioFour = (RadioButton) findViewById(R.id.radio_button_four_yogurt_question_eight);
        RadioButton radioFive = (RadioButton) findViewById(R.id.radio_button_five_sector_question_eight);
        String questionEightCorrect = "humane";
        switch (view.getId()) {
            case R.id.radio_button_one_ginger_question_eight:
                if (radioChecked) {
                    if (questionEightCorrect.equalsIgnoreCase(radioOne.getText().toString())) {
                        questionEightUserAnswer = true;
                        radioOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionEightEnabledViews();
                        displayFullScore();
                    } else {

                        questionEightUserAnswer = false;
                        radioOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionEightEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_two_humane_question_eight:
                if (radioChecked) {
                    if (questionEightCorrect.equalsIgnoreCase(radioTwo.getText().toString())) {
                        questionEightUserAnswer = true;
                        radioTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionEightEnabledViews();
                        displayFullScore();
                    } else {

                        questionEightUserAnswer = false;
                        radioTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionEightEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_three_abacus_question_eight:
                if (radioChecked) {
                    if (questionEightCorrect.equalsIgnoreCase(radioThree.getText().toString())) {
                        questionEightUserAnswer = true;
                        radioThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionEightEnabledViews();
                        displayFullScore();
                    } else {

                        questionEightUserAnswer = false;
                        radioThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionEightEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_four_yogurt_question_eight:
                if (radioChecked) {
                    if (questionEightCorrect.equalsIgnoreCase(radioFour.getText().toString())) {
                        questionEightUserAnswer = true;
                        radioFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionEightEnabledViews();
                        displayFullScore();
                    } else {

                        questionEightUserAnswer = false;
                        radioFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionEightEnabledViews();
                    }
                }
                break;
            case R.id.radio_button_five_sector_question_eight:
                if (radioChecked) {
                    if (questionEightCorrect.equalsIgnoreCase(radioFive.getText().toString())) {
                        questionEightUserAnswer = true;
                        radioFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                        questionEightEnabledViews();
                        displayFullScore();
                    } else {

                        questionEightUserAnswer = false;
                        radioFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                        questionEightEnabledViews();
                    }
                }
                break;
        }
        scrollToView( R.id.question_eight_layout);
    }
    //THe End Of Question Eight

    //Start Of Question Nine
    public void questionNine(View view) {
        numberOfQuestionsAnswered++;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        int correctAnswer = 198;
        EditText editText = (EditText) findViewById(R.id.edit_text_one_question_nine);
        String getAnswer = editText.getText().toString();
        Button submitButton = (Button) findViewById(R.id.button_one_question_nine);

        if (getAnswer.matches("")) {
            Toast.makeText(getApplicationContext(), "Enter Your Answer !!", Toast.LENGTH_LONG).show();
        } else {
            if (correctAnswer == Integer.parseInt(getAnswer)) {
                questionNineUserAnswer = true;
                editText.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                editText.setEnabled(false);
                submitButton.setEnabled(false);
                displayFullScore();
            } else {
                questionNineUserAnswer = false;
                editText.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                editText.setEnabled(false);
                submitButton.setEnabled(false);
            }
            scrollToView( R.id.question_nine_layout);
        }
    }

    //Process In Question Ten
    public void questionTenEnabledViews() {
        CheckBox checkOne = (CheckBox) findViewById(R.id.checkbox_one_imaginary_question_ten);
        CheckBox checkTwo = (CheckBox) findViewById(R.id.checkbox_two_realistic_question_ten);
        CheckBox checkThree = (CheckBox) findViewById(R.id.checkbox_three_illegible_question_ten);
        CheckBox checkFour = (CheckBox) findViewById(R.id.checkbox_four_impracticable_question_ten);
        CheckBox checkFive = (CheckBox) findViewById(R.id.checkbox_five_radical_question_ten);
        CheckBox checkSix = (CheckBox) findViewById(R.id.checkbox_six_embellished_question_ten);
        Button submitButton = (Button) findViewById(R.id.button_one_submit_question_ten);
        checkOne.setEnabled(false);
        checkTwo.setEnabled(false);
        checkThree.setEnabled(false);
        checkFour.setEnabled(false);
        checkFive.setEnabled(false);
        checkSix.setEnabled(false);
        submitButton.setEnabled(false);
    }

    //Start Of Question Ten
    public void questionTen(View view) {
        numberOfQuestionsAnswered++;
        int numberOfCheckBoxs = 0;
        Log.v(TAG, String.valueOf(numberOfQuestionsAnswered));
        CheckBox checkOne = (CheckBox) findViewById(R.id.checkbox_one_imaginary_question_ten);
        boolean isCheckedOne = checkOne.isChecked();
        CheckBox checkTwo = (CheckBox) findViewById(R.id.checkbox_two_realistic_question_ten);
        boolean isCheckedTwo = checkTwo.isChecked();
        CheckBox checkThree = (CheckBox) findViewById(R.id.checkbox_three_illegible_question_ten);
        boolean isCheckedThree = checkThree.isChecked();
        CheckBox checkFour = (CheckBox) findViewById(R.id.checkbox_four_impracticable_question_ten);
        boolean isCheckedFour = checkFour.isChecked();
        CheckBox checkFive = (CheckBox) findViewById(R.id.checkbox_five_radical_question_ten);
        boolean isCheckedFive = checkFive.isChecked();
        CheckBox checkSix = (CheckBox) findViewById(R.id.checkbox_six_embellished_question_ten);
        boolean isCheckedSix = checkSix.isChecked();
        if (isCheckedOne)
            numberOfCheckBoxs++;
        if (isCheckedTwo)
            numberOfCheckBoxs++;
        if (isCheckedThree)
            numberOfCheckBoxs++;
        if (isCheckedFour)
            numberOfCheckBoxs++;
        if (isCheckedFive)
            numberOfCheckBoxs++;
        if (isCheckedSix)
            numberOfCheckBoxs++;

        if (numberOfCheckBoxs > 2) {
            Toast.makeText(getApplicationContext(), "Choose Only Two Answers !!", Toast.LENGTH_LONG).show();
        } else {
            if (isCheckedOne) {
                if (isCheckedTwo) {
                    // One And Two Are Checked
                    questionTenUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else if (isCheckedThree) {
                    //One And Three Are Checked
                    questionTenUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                } else if (isCheckedFour) {
                    //One And Four Are Checked
                    questionTenUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else if (isCheckedFive) {
                    //One And Five Are Checked
                    questionTenUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();

                } else if (isCheckedSix) {
                    //One And Six Are Checked
                    questionTenUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                } else {
                    //only One Has Checked
                    questionTenUserAnswer = false;
                    checkOne.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                }
            } else if (isCheckedTwo) {
                if (isCheckedThree) {
                    //Two And Three Are Checked
                    questionTenUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else if (isCheckedFour) {
                    //Two And Four Are Checked
                    questionTenUserAnswer = true;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTenEnabledViews();
                    displayFullScore();
                } else if (isCheckedFive) {
                    //Two And Five Are Checked
                    questionTenUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else if (isCheckedSix) {
                    //Two And Six Are Checked
                    questionTenUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else {
                    //only Two Has Checked
                    questionTenUserAnswer = false;
                    checkTwo.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                }
            } else if (isCheckedThree) {
                if (isCheckedFour) {
                    //Three And Four Are Checked
                    questionTenUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else if (isCheckedFive) {
                    //Three And Five Are Checked
                    questionTenUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                } else if (isCheckedSix) {
                    //Three And Six Are Checked
                    questionTenUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                } else {
                    //only Three Has Checked
                    questionTenUserAnswer = false;
                    checkThree.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                }
            } else if (isCheckedFour) {
                if (isCheckedFive) {
                    //Four And Five Are Checked
                    questionTenUserAnswer = false;
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else if (isCheckedSix) {
                    //Four And Six Are Checked
                    questionTenUserAnswer = false;
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                } else {
                    //only Four Has Checked
                    questionTenUserAnswer = false;
                    checkFour.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.rightAnswer));
                    questionTenEnabledViews();
                    displayHalfScore();
                }
            } else if (isCheckedFive) {
                if (isCheckedSix) {
                    //Five And Six Are Checked
                    questionTenUserAnswer = false;
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                } else {
                    //only Five Has Checked
                    questionTenUserAnswer = false;
                    checkFive.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                    questionTenEnabledViews();
                }
            } else if (isCheckedSix) {
                //only Six Has Checked
                questionTenUserAnswer = false;
                checkSix.setTextColor(ContextCompat.getColorStateList(getApplicationContext(), R.color.wrongAnswer));
                questionTenEnabledViews();
            }
            scrollToView( R.id.question_ten_layout);
        }
    }


    public void displayCorrectAnswer() {
        String finalCorrectAnswer = "";
        if (!questionOneUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "Q 1) " + getString(R.string.question_one_correct_answer) + "\n";
        }
        if (!questionTwoUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 2) " + getString(R.string.question_two_correct_answer) + "\n";
        }
        if (!questionThreeUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 3) " + getString(R.string.question_three_correct_answer) + "\n";
        }
        if (!questionFourUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 4) " + getString(R.string.question_four_correct_answer) + "\n";
        }
        if (!questionFiveUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 5) " + getString(R.string.question_five_correct_answer) + "\n";
        }
        if (!questionSixUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 6) " + getString(R.string.question_six_correct_answer) + "\n";
        }
        if (!questionSevenUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 7) " + getString(R.string.question_seven_correct_answer) + "\n";
        }
        if (!questionEightUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 8) " + getString(R.string.question_eight_correct_answer) + "\n";
        }
        if (!questionNineUserAnswer) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 9) " + getString(R.string.question_nine_correct_answer) + "\n";
        }
        if (!(questionTenUserAnswer)) {
            finalCorrectAnswer = finalCorrectAnswer + "\nQ 10) " + getString(R.string.question_ten_correct_answer) + "\n";
        }
        TextView textViewOne = (TextView) findViewById(R.id.resultStaticTextView);
        textViewOne.setVisibility(View.VISIBLE);
        TextView textViewTwo = (TextView) findViewById(R.id.text_view_final_result);
        textViewTwo.setText(finalCorrectAnswer);
        textViewTwo.setVisibility(View.VISIBLE);


    }

    public void resultButton(View view) {
        if (numberOfQuestionsAnswered < 10) {
            Toast.makeText(getApplicationContext(), "Answer All Questions Above", Toast.LENGTH_SHORT).show();
        } else {
            displayReview();
            final ScrollView scrollView = (ScrollView)findViewById(R.id.scroll_view_id);
            displayCorrectAnswer();
            final Button result=(Button) findViewById(R.id.button_final_result);
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0 , result.getBottom());
                }
            });
        }
    }

    public void displayFullScore() {
        Score = Score + 1;
        TextView scoreTextView = (TextView) findViewById(R.id.text_view_score);
        scoreTextView.setText(String.valueOf(Score));
    }

    public void displayHalfScore() {
        Score = Score + 0.5f;
        TextView scoreTextView = (TextView) findViewById(R.id.text_view_score);
        scoreTextView.setText(String.valueOf(Score));
    }
    public void displayReview()
    {
        String review;
        if(Score >= 8) {
            review = "Good Job " + Name;
            review = review + "\n You Answered "+(int) Score +"from 10 Questions";
            review = review + "\nif You Wan't To learn More You Can Check This Book \n\"The Complete Book Of Intelligence Tests";
            review = review + "\t\tAnd Thanks For Your Time";
        }else if(Score < 8 && Score >=6){
            review = "Not Bad " + Name;
            review = review + "\n You Answered "+(int) Score +"from 10 Questions";
            review = review + "\n You Can Check This Book \n\"The Complete Book Of Intelligence Tests\" To Learn More IQ ...";
            review = review + "\t\tAnd Thanks For Your Time";
        }else
        {
            review = "Hi " + Name;
            review = review + "\n You Answered "+(int) Score +"from 10 Questions";
            review = review + "\n You Can Check This Book \n\"The Complete Book Of Intelligence Tests\" To Learn More IQ And Come Back Again To Check Your powerful...";
            review = review + "\t\tWe Wait You "+Name +" To Be The Best Of Our Talent";
        }
        TextView textView = (TextView)findViewById(R.id.display_review);
        textView.setText(review);
        textView.setVisibility(View.VISIBLE);
    }
}